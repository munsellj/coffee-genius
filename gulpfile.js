var gulp = require('gulp'),
	concat = require('gulp-concat'),
	connect = require('gulp-connect'),
	jshint = require('gulp-jshint'),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify');

// Local Variables
var paths = {
	assets: ['./index.html'],
	img: [
		'./img/favicon.ico',
		'./img/**/*.png'
	],
	js: [
		'./src/main.js',
		'./src/**/*.js'
	],
	libs: [
		'./bower_components/angular-resource/angular-resource.min.js',
		'./bower_components/moment/min/moment.min.js'
	],
	public: './public',
	// source styles used for watch, only main.scss used for compilation
	styles: ['./src/**/*.scss']
};

gulp.task('build', ['js-libs', 'js', 'sass', 'copy-assets']);

gulp.task('connect', function() {
  connect.server({root: 'public'});
});

gulp.task('copy-assets', function () {
	gulp.src(paths.assets)
    	.pipe(gulp.dest(paths.public));

	gulp.src(paths.img)
    	.pipe(gulp.dest(paths.public + '/img'));
});

gulp.task('js', function() {
	return gulp.src(paths.js)
		.pipe(jshint('.jshintrc'))
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(uglify())
		.pipe(concat('app.js'))
		.pipe(gulp.dest(paths.public + '/js'));
});

gulp.task('js-libs', function () {
	gulp.src(paths.libs)
		.pipe(concat('libs.js'))
		.pipe(gulp.dest(paths.public + '/js'));
});

gulp.task('sass', function () {
	// Use main file as source for compilation
	gulp.src(paths.styles)
		.pipe(sass({errLogToConsole: true}))
		.pipe(gulp.dest(paths.public + '/css'));
});

/*gulp.task('uglify', function () {
	gulp.src(paths.js)
		.pipe(uglify())
		.pipe(gulp.dest(paths.public + '/js'));
});*/

gulp.task('watch', function(){
	gulp.watch(paths.js, ['js']);
	gulp.watch(paths.styles, ['sass']);
	gulp.watch(paths.assets, ['copy-assets']);
});

gulp.task('default', ['build', 'connect', 'watch']);
