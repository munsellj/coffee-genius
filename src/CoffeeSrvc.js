'use strict';
/**
 * @ngdoc service
 * @name CoffeeSrvc
 * @function
 *
 * @requires $q
 * @requires $resource
 * @requires AppConfig
 *
 * @returns {object} The service's public interface
 *
 * @description
 * Service for interacting with the coffee genius api.
 */
angular.module('coffee-genius').factory('CoffeeSrvc', [
		'$q',
		'$resource',
		'AppConfig',
	function CoffeeSrvcFactory(
		$q,
		$resource,
		AppConfig
	) {

		// Service Instance
		var CoffeeSrvc = {};

		/**
		 * NOTE: Add empty data body since jsonstubrequires it for all
		 * requests but angular removes by default for GET requests
		 * with no request body.
		 */
		var CoffeeResource = $resource(
			AppConfig.apiBaseUrl + '/roast/:roast_name',
			{'roast_name': '@roast_name'},
			{
				getRoasts: {
					data: '',
					method: 'GET',
					timeout: AppConfig.httpTimeout,
					url: AppConfig.apiBaseUrl + '/roasts'
				},
				getRoastDetails: {
					data: '',
					method: 'GET',
					timeout: AppConfig.httpTimeout
				}
			}
		);


		// Public Methods
		// -------------------------

		/**
		 * @ngdoc method
		 * @name getRoasts
		 * @methodOf CoffeeSrvc
		 *
		 * @returns {Promise}
		 *
		 * @description
		 * Get available coffee Roasts.
		 */
		CoffeeSrvc.getRoasts = function getRoasts() {
			var deferred = $q.defer();

			CoffeeResource.getRoasts(
				{},
				function getRoastsSuccess(response) {
					deferred.resolve(response.roasts);
				},
				deferred.reject
			);

			return deferred.promise;
		};

		/**
		 * @ngdoc method
		 * @name getRoastDetails
		 * @methodOf CoffeeSrvc
		 *
		 * @param {string} roastName - The name of the roast to get details for.
		 *
		 * @returns {Promise}
		 *
		 * @description
		 * Gets the details for a given roast.
		 */
		CoffeeSrvc.getRoastDetails = function getRoastDetails(roastName) {
			var deferred = $q.defer();

			CoffeeResource.getRoastDetails(
				{roast_name: roastName},
				deferred.resolve,
				deferred.reject
			);

			return deferred.promise;
		};


		// Return Service Instance
		return CoffeeSrvc;
	}
]);
