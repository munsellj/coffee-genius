'use strict';
/**
 * @ngdoc function
 * @name MainCtrl
 *
 * @requires $scope
 * @requires $timeout
 * @requires CoffeeSrvc
 *
 * @description
 * The Main / Root application controller.
 */
angular.module('coffee-genius').controller('MainCtrl', [
		'$scope',
		'$timeout',
		'CoffeeSrvc',
	function MainCtrl(
		$scope,
		$timeout,
		CoffeeSrvc
	) {

		// Controller Variables
		// ---------------------------------

		var selectedRoastData = null;


		// Scope Variables
		// ---------------------------------

		$scope.coffeeAmountConfig = {
			increments: 50,
			max: 1000,
			min: 250
		};
		$scope.roasts = [];


		// Controller Functions
		// ---------------------------------

		function init() {
			// init range value on timeout
			$timeout(function() {
				// init range to 1/2 of max
				var rangeIncrementCount = ($scope.coffeeAmountConfig.max - $scope.coffeeAmountConfig.min) / $scope.coffeeAmountConfig.increments;

				$scope.coffeeAmount = (Math.floor(rangeIncrementCount / 2) * $scope.coffeeAmountConfig.increments) + $scope.coffeeAmountConfig.min;
			});


			CoffeeSrvc.getRoasts().then(
				function getRoastsSuccess(response) {
					$scope.roasts = response;

					if ($scope.roasts.length) {
						$scope.selectedRoast = $scope.roasts[0];
					}
					else {
						$scope.roastsError = 'Oops! Looks like there was an issue loading the roasts :-(';
					}
				},
				function getRoastsError() {
					$scope.roastsError = 'Oops! Looks like there was an issue loading the roasts :-(';
				}
			);
		}

		function onRoastChange() {
			// do nothing when there is no selected value
			if (!$scope.selectedRoast) {
				return;
			}

			// Get Roast Details
			CoffeeSrvc.getRoastDetails($scope.selectedRoast).then(
				function getRoastDetailsSuccess(response) {
					selectedRoastData = response;

					updateBrewingGuide();
				},
				function getRoastDetailsError() {
					$scope.roastDetailsError = 'Oops! Looks like there was an issue loading the roast details :-(';
				}
			);
		}

		function updateBrewingGuide() {
			/**
			 * Try updating the brewing guide, catch any exceptions in case of
			 * missing/bad data and show error.
			 */
			try {
				var beansRatio,
					brewTimeMoment = moment({seconds: selectedRoastData.brewTime}),
					brewTimeText = '',
					minutes = brewTimeMoment.minutes(),
					ratios = selectedRoastData.ratio.split(':'),
					seconds = brewTimeMoment.seconds(),
					waterRatio;

				beansRatio = ratios[0];
				waterRatio = ratios[1];

				brewTimeText += minutes + ((minutes > 1) ? ' minutes ' : ' minute ') +
					seconds + ((seconds > 1) ? ' seconds' : ' second');

				$scope.roastDetails = {
					beanAmount: (beansRatio * Math.floor($scope.coffeeAmount / waterRatio)) + 'g',
					brewTime: brewTimeText,
					ratio: selectedRoastData.ratio
				};
			}
			catch (e) {
				$scope.roastDetailsError = 'Oops! Looks like there was an issue loading the roast details :-(';
			}
		}

		// Scope Functions
		// ---------------------------------

		$scope.onAmountChange = function onAmountChange(evt) {
			if (evt && angular.isFunction(evt.stopImmediatePropagation)) {
				evt.stopImmediatePropagation();
			}

			updateBrewingGuide();
		};

		$scope.$watch('selectedRoast', onRoastChange);

		// Initialize
		init();
	}
]);
