'use strict';

;(function main(angular) {

	// App module definition
	angular.module('coffee-genius', [
		'ngResource'
	])

	// App Configuration
	.constant('AppConfig', {
		apiBaseUrl: 'http://jsonstub.com/coffeegenius',
		httpTimeout: 50000
	})

	// Global HTTP Config
	.config([
		'$httpProvider',
		function config_httpProvider($httpProvider) {
			// set $httpProvider default request headers
			angular.extend($httpProvider.defaults.headers.common, {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			});

			// Register a HTTP error response interceptor via an anonymous factory
			$httpProvider.interceptors.push([
					'$q',
					'AppConfig',
				function httpProviderInterceptors(
					$q,
					AppConfig
				) {
					return {
						request: function requestInterceptor(config) {
							// Add required request headers for calls to API
							if (config.url.indexOf(AppConfig.apiBaseUrl) > -1) {
								config.headers['JsonStub-User-Key'] = '0cb331ba-d888-4397-a5d4-e3c98bb197fa';
								config.headers['JsonStub-Project-Key'] = '05ffebc9-7519-4efc-8ba1-5d21a07cae5d';
							}

							return config;
						},
						responseError: function responseErrorInterceptor(rejection) {
							// TODO: API global error handling here

							return $q.reject(rejection);
						}
					};
				}
			]);
		}
	])

	// Override Angular exception handler
	.factory('$exceptionHandler', function override_exceptionHandler() {
		return function (exception, cause) {
			// make it clear the exception / error occurred in Angular
			if (exception.message) {
				exception.message = '[Angular Exception]: ' + exception.message;
			}
			else {
				exception.message = '[Angular Exception] - caused by: ' + cause;
			}

			console.error(exception, cause);
		};
	});

})(angular);
